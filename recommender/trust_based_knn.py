import numpy as np
from surprise import AlgoBase
from surprise import Dataset
from surprise import evaluate


class TrustKNN(AlgoBase):
    def __init__(self):
        AlgoBase.__init__(self)
        self.user_means = {}
        self.item_means = {}

    def train(self, trainset):
        AlgoBase.train(self, trainset)
        for uiid in self.trainset.ur.keys():
            self.user_means[uiid] = np.mean(self.trainset.ur[uiid])
        
        for iiid in self.trainset.ir.keys():
            self.item_means[iiid] = np.mean(self.trainset.ir[iiid])
           

    def estimate(self, u, i):
        sum_means = 0

        if self.trainset.knows_user(u):
            sum_means += self.user_means[u]
            
        if self.trainset.knows_item(i):
            sum_means += self.item_means[i]
            
        return sum_means / 2.0



if __name__ == '__main__':
    data = Dataset.load_builtin('ml-100k')
    model = TrustKNN()

    evaluate(model, data)