# rating model
# https://arxiv.org/pdf/1703.01049.pdf
# the rating made by user u of item i is given by
# r_ui = L(a_u + b_u * t_i + e_ui)
# where a_u is the center of user u's rating scale,
# b_u is the rating sensitivity of user u, 
# t_i the instrinsic score of item i,
# e_ui is noise in the rating process
#
# L is the discrete levels function defined by
# L(r) = max(min(round(r), 5), 1) 
# All ratings are between 1 and 5 (inclusive)
import numpy as np
from scipy import sparse 
from scipy import stats 

L = lambda r: max(min(round(r), 5), 1) 
# sparsity coefficient
sparsity_level = 0.01
noise_level = 0.1

n_users = 1024
n_items = 10000

# center of user u's rating scale
# a_u ~ N(3, 1)
center = np.random.normal(3, 1, size=n_users)
# rating sensitivity of user u
# b_u ~ N(0.5, 0.5) 
sensitivity = np.random.normal(0.5, 0.5, size=n_users)

# instrinsic score of item i
# t_i ~ N(0.1, 1)
instrinsic_score = np.random.normal(0.1, 1, size=n_items)
# noise in rating
# e_ui ~ N(0, 1)
noise = stats.norm(0, 1).rvs


ratings_matrix = sparse.csr_matrix((n_users, n_items), 
                                   dtype=np.int8)

n_ratings = int(sparsity_level * n_users * n_items)

for ix in np.random.choice(xrange(n_users*n_items), size=n_ratings, replace=False):
    user, item = divmod(ix, n_items)
    # r_ui = L(a_u + b_u * t_i + e_ui)
    rating = L(center[user] + sensitivity[user] * instrinsic_score[item] +\
                    noise_level * noise())
    ratings_matrix[user, item] = rating

sparse.save_npz('data/ratings_matrix.npz', ratings_matrix)