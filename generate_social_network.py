# -*- coding: utf-8 -*-
import numpy as np
import networkx as nx
from network_generators.product import kronecker_random_graph 

if __name__ == '__main__':
    # generate a Stochastic Kronecker graph with
    # charactersitics similar to the Epinions graph
    # https://cs.stanford.edu/people/jure/pubs/kronecker-jmlr10.pdf
    probability_matrix = [[0.99, 0.54], 
                          [0.49, 0.13]]
    k = 10

    # social graph with 2^k nodes
    social_network = kronecker_random_graph(k, probability_matrix)
    # set edge weights to 1.0
    for u, v, d in social_network.edges(data=True):
        d['trust'] = 1.0
    # store network as edge-list
    nx.write_edgelist(social_network, 'data/social_network.edgelist')
