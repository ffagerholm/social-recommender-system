import numpy as np
import networkx as nx 


def trust_metric(path_length, cutoff=5):
    if path_length > cutoff:
        return 0.0
    else:
        return float(cutoff - path_length + 1) / cutoff


def augment_network(network):
    path_lengths = nx.shortest_path_length(network, weight='trust')
    augment_network = network.copy()

    for source_node in path_lengths.keys():
        for target_node, path_length in \
                path_lengths[source_node].items():
            if path_length >= 2:
                propagated_trust = trust_metric(path_length)
                augment_network.add_edge(source_node, 
                                 target_node, 
                                 trust=propagated_trust)
    return augment_network


if __name__ == '__main__':
    social_network = nx.read_edgelist('data/social_network.edgelist', 
                                      create_using=nx.DiGraph())
    augmented_network = augment_network(social_network)
    nx.write_edgelist(augmented_network, 'data/augmented_network.edgelist')
