import csv
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt

social_graph = nx.read_weighted_edgelist('data/user_rating_subset_nonnegative.csv', 
                                          delimiter='\t',
                                          create_using=nx.DiGraph())


"""

social_graph = nx.DiGraph()
social_graph.add_node(0, pos=(0, 0))
social_graph.add_node(1, pos=(0, 1))
social_graph.add_node(2, pos=(1, 1))
social_graph.add_node(3, pos=(1, 0))

social_graph.add_edge(0, 1, trust=1)
social_graph.add_edge(1, 2, trust=1)
social_graph.add_edge(2, 3, trust=1)
"""

plt.figure()
positions = nx.spring_layout(social_graph)
nx.draw_networkx(social_graph, 
                 pos=positions,
                 with_labels=True)
labels = nx.get_edge_attributes(social_graph, 'trust')
nx.draw_networkx_edge_labels(social_graph, 
                             pos=positions,
                             edge_labels=labels)


augmented_social_graph = social_graph.copy()

for start_node, paths_dict in\
        nx.all_pairs_shortest_path(social_graph, cutoff=3).items():
    for end_node, path in paths_dict.items():
        if len(path) > 2:
            augmented_social_graph.add_edge(start_node, end_node, 
                                            trust=pow(0.5, len(path) - 2))


plt.figure()
nx.draw_networkx(augmented_social_graph, 
                 pos=positions,
                 with_labels=True)
labels = nx.get_edge_attributes(augmented_social_graph, 'trust')
nx.draw_networkx_edge_labels(augmented_social_graph, 
                             pos=positions,
                             edge_labels=labels)


plt.show()